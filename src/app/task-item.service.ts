import { Injectable } from '@angular/core';
import {TaskItem} from "./task-item";

@Injectable({
  providedIn: 'root'
})
export class TaskItemService {

  tasks: TaskItem[] = [];

  constructor() {
    this.loadTaskItems();
    if (this.tasks.length === 0) {
      this.tasks = this.initTaskItems();
    }
  }

  addTaskItem(taskItem: TaskItem) {
    this.tasks.push(taskItem);
    this.persistTaskItems();
  }

  removeTaskItem(taskItemToRemove: TaskItem): void {
    this.tasks = this.tasks.filter((taskItem: TaskItem) => taskItem !== taskItemToRemove);
    this.persistTaskItems()
  }

  getOpenTasks(): TaskItem[] {
    const openTasks = this.tasks.filter((taskItem: TaskItem) => !taskItem.done);
    return this.sortTasks(openTasks);
  }

  getClosedTasks(): TaskItem[] {
    const closedTasks = this.tasks.filter((taskItem: TaskItem) => taskItem.done);
    return this.sortTasks(closedTasks);
  }

  persistTaskItems(): void {
    localStorage.setItem('tasks', JSON.stringify(this.tasks));
  }

  private sortTasks(taskItems: TaskItem[]) {
    return taskItems.sort((taskItemA: TaskItem, taskItemB: TaskItem) => taskItemA.dueDate.getTime() - taskItemB.dueDate.getTime());
  }

  private initTaskItems(): TaskItem[] {
    const items: TaskItem[] = [
      {
        title: 'Ausbildungsbeginn bei der Debeka',
        description: 'Das klingt spannend...',
        dueDate: new Date(2024,8,1),
        done: false
      },
      {
        title: 'Summerschool 2024',
        description: 'Toller Vortrag zu CICD...',
        dueDate: new Date(2024,3,6),
        done: false
      }
    ];
    return items;
  }


  private loadTaskItems(): void {
    const itemsString = localStorage.getItem('tasks');
    if (itemsString) {
      const itemsArray: any[] = JSON.parse(itemsString);
      itemsArray.map((item: any) => {
        item.dueDate = new Date(item.dueDate);
        return item;
      });
      this.tasks = itemsArray as TaskItem[];
    }
  }

}
